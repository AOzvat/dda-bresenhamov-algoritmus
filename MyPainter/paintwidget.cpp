#include "paintwidget.h"


PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::kresli_body(int body, double polomer) {
	int xs = image.width();
	int ys = image.height();

	QPainter painter(&image);
	painter.setPen(QPen(QColor("black"), myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

	double delenia = 2.0 * M_PI / body;

	body_x.resize(body);
	body_y.resize(body);

	for (int i = 0; i < body; i++) {
		double uhol = delenia * i;

		int x = (int)((xs / 2) + (polomer * cos(uhol)));
		int y = (int)((ys / 2) + (polomer * sin(uhol)));

		body_x[i] = x;
		body_y[i] = y;

		painter.drawPoint(x, y);
	}
	update();
}

void PaintWidget::nastav_farbu(int red, int green, int blue, int a) {
	farba.setRgb(red, green, blue, a);
}

void PaintWidget::dda() {
	int x1, y1, x2, y2, dx, dy, krok;
	float zvacsene_x, zvacsene_y, x, y;

	QPainter painter(&image);
	painter.setPen(QPen(farba, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	//prejde vsetky body
	for (int i = 0; i < body_x.size(); i++) {
		//zaradi do premennych body
		if (i != body_x.size() - 1) {
			x1 = body_x[i];
			y1 = body_y[i];
			x2 = body_x[i + 1];
			y2 = body_y[i + 1];
		}
		else {
			x1 = body_x[i];
			y1 = body_y[i];
			x2 = body_x[0];
			y2 = body_y[0];
		}
		
		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//pre zaciatok pripadi zaciatocny bod
		x = x1;
		y = y1;

		//rozhodne sa v ktorom smere sa bude krokuvat
		if (abs(dx) > abs(dy)) {
			krok = abs(dx);
		}
		else {
			krok = abs(dy);
		}

		//o kolko sa budu suradnice zvacsovat
		// if dx > dy -> zvacsenie_x = 1
		// if dx < dy -> zvacsenie_y = 1
		zvacsene_x = dx / (float)krok;
		zvacsene_y = dy / (float)krok;

		//zvacsovanie a vykreslovanie "ciare"
		for (int j = 0; j < krok; j++) {
			x += zvacsene_x;
			y += zvacsene_y;
			painter.drawPoint(x, y);
		}
	}
	
	update();
}

void PaintWidget::bresen() {
	int x1, y1, x2, y2, dx, dy, dx1, dy1, krok;
	int x, y;
	float chyba;
	int k1, k2, p;

	QPainter painter(&image);
	painter.setPen(QPen(farba, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	
	//prejde vsetky body
	for (int i = 0; i < body_x.size(); i++) {
		//zaradi do premennych body
		if (i != body_x.size() - 1) {
			x1 = body_x[i];
			y1 = body_y[i];
			x2 = body_x[i + 1];
			y2 = body_y[i + 1];
		}
		else {
			x1 = body_x[i];
			y1 = body_y[i];
			x2 = body_x[0];
			y2 = body_y[0];
		}

		//vypocita rozdiel medzi bodmi
		dx = x2 - x1;
		dy = y2 - y1;

		//aby sme kreslili vzdy iba ciaru do 45 stupnov, nie strmsiu
		if (abs(dy) > abs(dx)) {
			std::swap(x1, y1);
			std::swap(x2, y2);
		}

		//aby sme vzdy kreslili ciaru s lava na pravo
		if (x1 > x2) {
			std::swap(x1, x2);
			std::swap(y1, y2);
		}

		//vypocita rozdiel medzi vymenenymi bodmi
		dx1 = x2 - x1;
		dy1 = y2 - y1;

		chyba = dx1 / 2.0f;

		//alebo sa ciara bude kreslit dolu alebo hore vzhladom na y
		if (y1 < y2) {
			krok = 1;
		}
		else {
			krok = -1;
		}

		y = y1;
/*
		k1 = 2 * dy1;
		k2 = 2 * dy1 - 2 * dx1;
		p = 2 * dy1 - dx1;

		for (int x = x1; x < x2; x++) {
			if (p > 0) {
				y += krok;
				p += k2;
			}
			else {
				//nieco pre y aby sa zvacsovalo pri nejakej podmienky
				//if () {
				//
				//}
				p += k1;
			}

			//ak sa hore kvoli strmosti ciare zamenili x a y, teraz to vratime spat
			//kreslime y, x a ak nie, tak normalne x, y
			if (abs(dy) > abs(dx)) {
				painter.drawPoint(y, x);
			}
			else {
				painter.drawPoint(x, y);
			}
		}
*/

		//zvacsuje x vzdy o 1
		for (int x = x1; x < x2; x++){
			//ak sa hore kvoli strmosti ciare zamenili x a y, teraz to vratime spat
			//kreslime y, x a ak nie, tak normalne x, y
			if (abs(dy) > abs(dx)){
				painter.drawPoint(y, x);
			}
			else{
				painter.drawPoint(x, y);
			}

			chyba -= abs(dy1);
			if (chyba < 0){
				y += krok;
				chyba += dx1;
			}
		}
	}
	
	update();
}