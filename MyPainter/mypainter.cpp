#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::KresliClicked()
{
	//vymaze kresliacu plochu
	paintWidget.clearImage();

	//podeli kruznicu a nakresli body v dobrom polomere
	vykresli_body();
	
	//nastavi zadanu farbu
	nastavi_farbu();

	//DDA
	if (ui.comboBox->currentIndex() == 0) {
		paintWidget.dda();
	}
	
	//Bresen
	if (ui.comboBox->currentIndex() == 1) {
		paintWidget.bresen();
	}
}

void MyPainter::vykresli_body() {
	if (ui.lineEdit_2->text() == "") {
		paintWidget.kresli_body(ui.spinBox->value());
	}
	else {
		paintWidget.kresli_body(ui.spinBox->value(), ui.lineEdit_2->text().toDouble());
	}
}

void MyPainter::nastavi_farbu() {
	if (ui.lineEdit_3->text() == "" && ui.lineEdit_4->text() == "" && ui.lineEdit_5->text() == "") {
		paintWidget.nastav_farbu();
	}
	else {
		paintWidget.nastav_farbu(ui.lineEdit_3->text().toInt(), ui.lineEdit_4->text().toInt(), ui.lineEdit_5->text().toInt());
	}
}